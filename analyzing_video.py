#Importing Libraries
import numpy as np
import cv2
import matplotlib.pyplot as plt 

VFILE = "video/hot_rod.mp4"

#Defining Generator for reading video
def get_frames(filename):                        #It's a iterator that pulls a frame one at a time
    video = cv2.VideoCapture(filename)           # Instantiating a video capture object
    while video.isOpened():                      # Start a loop after checking video file is opened
        ret,frame = video.read()                 # reading a video returns tuple first one is bool tells whether a new frame is  
        if ret:                                  # retrieved, will be automatically false at the end of videoand other one is frame of video
            yield frame                          
        else:
            break;                               #breaking at the end of video
    video.release()                              #releasing the resources
    yield None                            

'''yield is a keyword in Python that is used to return from a
function without destroying the states of its local variable and 
when the function is called, the execution starts from the last 
yield statement. Any function that contains a yield keyword is 
termed as generator'''

